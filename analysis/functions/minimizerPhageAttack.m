close all; clearvars;

p = gcp('nocreate');
if isempty(p)
    parpool('local', 32);
end

% Select model
model = 1;

%    dR        epsilon   nu
lb = [1        0.05      0  ];
y0 = [4        12.5      0  ];
ub = [20       50.0      0  ];


% Load the best fitting parameters (bacteria parameters)
load('../fits/GrowthParams.mat')

% Load the fitting data
load('../../experiments/PhageData.mat', 'dataset')

% Allocate cells for fitting data
tmp1 = [dataset{:, 1}];
tmp2 = [dataset{:, 2}];
tmp3 = [dataset{:, 3}];

T_i  = tmp1(1, :) + 5;

nd = length(dataset);
Time = cell(nd, 1);
BF   = cell(nd, 1);
GFP  = cell(nd, 1);
for d = 1:nd
    Time{d} = tmp1(:, d);
    BF{d}   = tmp2(:, d);
    GFP{d}  = tmp3(:, d);
end

% Create output folder
sdir = '../fits/PhageAttack';
if ~exist(sdir, 'dir')
    mkdir(sdir)
end

% Check for previous fit
path = sprintf('%s/Params.mat', sdir);
if exist(path, 'file')
    load(path, 'y');
    y0 = y(1:3);    % First 3 values are related to phage attack model
    T_i = y(4:end); % Remaining values contain the estimates for T_i
end

% Add T_i to fitting parameters
y0 = [y0 T_i];

% Determine start fit value
y_old = y0;
d_old = fitPhageAttack(x, y_old, model, Time, BF, GFP, false);

% FMINCON OR GENETIC ALGORITHM
lb = [lb   zeros(size(T_i))];
ub = [ub 24*ones(size(T_i))];

% Assert bounds are set
y0(y0 < lb) = lb(y0 < lb);
y0(y0 > ub) = ub(y0 > ub);

% Call minimizier
% FMINCON
%         options = optimoptions('fmincon', 'UseParallel', true);
%         y = fmincon(@(y)fitPhageAttack(x, y, model, Time, BF, GFP, detectionThreshold, true), y0, [], [], [], [], lb, ub, [], options);

% GENETIC ALGORITHM
%         options = optimoptions('ga', 'UseParallel', true);
%         y = ga(@(y)fitPhageAttack(x, y, model, Time, BF, GFP, true), numel(y0), [], [], [], [], lb, ub, [], options);

% FMINSEARCH
options = optimset('MaxIter', 200*numel(y0));
y = fminsearch(@(y)fitPhageAttack(x, y, model, Time, BF, GFP, true), y0, options);

% Report change in conditions
fprintf('Total parameter change: %.3f\n', norm(y-y_old))

d_new = fitPhageAttack(x, y, model, Time, BF, GFP, false);
fprintf('Fit improvement: %.3f %%\n', 100*(d_old-d_new)/d_old)

% Store fit
d = d_new;
save(path, 'y', 'd');


