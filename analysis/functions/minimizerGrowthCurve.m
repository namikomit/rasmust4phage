close all; clearvars;

% This script run a minimizer to find optimal fit values

% Running the fit without intrinsic decay of bacteria
%     n_0     log10(p.D)  g_max     K      delta    mu        R0
x0 = [0.007   6.07       4         0.57     0      0.75      1.4  ];

% Load the fitting data
load('../../experiments/GrowthData.mat', 'dataset')

T  = [dataset{:, 1}];
BF = [dataset{:, 2}];

% Get mean and error
t = T(:, 1);
mBF = nanmean(BF, 2);
dBF = nanstd(BF, [], 2) ./ sqrt(sum(~isnan(BF), 2));

% Filter out missing
I = isnan(mBF);
t(I) = [];
mBF(I) = [];
dBF(I) = [];

% Check for previous fit
path = sprintf('../fits/GrowthParams.mat');
if exist(path, 'file')
    load(path, 'x');
    x0 = x;
end

% Call minimizier
x = fminsearch(@(x)fitGrowthCurve(x, t, mBF, dBF, true), x0);

% Create output folder
if ~exist('../fits', 'dir')
    mkdir('../fits')
end

save('../fits/GrowthParams.mat', 'x');