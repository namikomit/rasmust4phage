close all; clearvars; clc;

% Load the fitting data
load('../../experiments/GrowthData.mat')
BF = [dataset{1}{2} dataset{2}{2} dataset{3}{2} dataset{4}{2} dataset{5}{2}];
T = dataset{1}{1};

% First dataset is only zeros
BF = BF(2:end, :);
T  = T(2:end);

% Get mean and error
BF(BF == 0) = nan;
mBF = nanmean(BF, 2);
dBF = nanstd(BF, [], 2) ./ sqrt(sum(~isnan(BF), 2));

% Load the fitted values
load('../fits/GrowthParams.mat');

% Get configration struct
p = getConfiguration(x);

% Load the fitting data
p.T_end = T;

% Solve model with current parameters
[t, C, ~, N, ~, p] = solveModel(p);

% Plot the best result
fh = figure(); clf; hold on;
ax = gca;
ax.FontSize = 20;
ax.LineWidth = 1;
ax.Box = 'on';

% Compute the velocity of expantion
v = C;
tt = t;
v(diff(C) == 0)  = [];
tt(diff(C) == 0) = [];

v = diff(v) ./ diff(tt);
tt = tt(1:(end-1)) + diff(tt) / 2;

% Determine the surface nutrient level
f = @(C)min(1,max(0, (C-p.r)./([p.r(2:end); p.rNp]-p.r)));
nC = nan(size(t));
for i = 1:numel(nC)
    fC = f(C(i));
    I = find(fC, 1, 'last');
    nC(i) = N(i, I-1) * (1-fC(I)) + N(i, I) * fC(I);
end

% Load the fitted values
load('../fits/PhageAttackParams.mat');
p = getConfiguration(x, y, 1);

yyaxis left
plot(ax, tt, v, 'LineWidth', 2);
ylabel('Colony growth rate ({\mu}m / h)')
ax.YLim = [0 100];

yyaxis right
plot(ax, t, 0.6*p.epsilon * p.dR * p.g_max * nC ./ (nC + p.K), 'LineWidth', 2);
ylabel('Phage killing rate ({\mu}m / h)')
ax.YLim = [0 100];

xlabel('Time (h)')

if ~exist('../../figures/Figure 5', 'dir')
    mkdir('../../figures/Figure 5')
end

saveas(fh, '../../figures/Figure 5/Fig5.png')


% Plot the different models
fh = figure(); clf; 
fh.Position([2 4]) = [100 600];
ax1 = subplot(2, 1, 1); hold on;
ax2 = subplot(2, 1, 2); hold on;
ax1.FontSize = 20;
ax2.FontSize = 20;
ax1.LineWidth = 1;
ax2.LineWidth = 1;
ax1.Box = 'on';
ax2.Box = 'on';

p.T_i = 8;
p.T_end = 24;

% Store values
epsilon = p.epsilon;
dR      = p.dR;

k = [0.1 1];

for i = 1:numel(k)
    
    p.epsilon = epsilon * k(i);
    p.dR      = dR / k(i);
    
    % Solve model with current parameters
    [t, C, R] = solveModel(p);

    % Plot individual data and fit
    plot(ax1, t, C, 'LineWidth', 2, 'DisplayName', sprintf('k = %.1f', k(i)));
    plot(ax2, t, R, 'LineWidth', 2, 'DisplayName', sprintf('k = %.1f', k(i)));
    
    pause(1)

end